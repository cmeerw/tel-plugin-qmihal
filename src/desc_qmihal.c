/*
 * tel-plugin-qmihal
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd. All rights reserved.
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Hayoon Ko <hayoon.ko@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>

#include <glib.h>

#include <tcore.h>
#include <plugin.h>
#include <hal.h>
#include <server.h>

#define SERVER_INIT_WAIT_TIMEOUT		500
#define MODEM_PLUGIN_NAME			"qmi-plugin.so"

typedef struct {
  TcoreHal *hal;
  TcoreModem *modem;
} PluginData;

static TReturn hal_power(TcoreHal *hal, gboolean enable)
{
  if (enable == TRUE)
  {
    /* Set Power State - ON */
    tcore_hal_set_power_state(hal, TRUE);
  }
  else
  {
    /* Set Power state - OFF */
    tcore_hal_set_power_state(hal, FALSE);
  }

  return TCORE_RETURN_SUCCESS;
}

/** HAL operations */
static struct tcore_hal_operations hal_ops = {
  .power = hal_power,
  .send = NULL,
  .setup_netif = NULL,
};

static gboolean load_modem_plugin(gpointer data)
{
  TcorePlugin *plugin = data;
  Server *s = tcore_plugin_ref_server(plugin);
  PluginData *user_data = tcore_plugin_ref_user_data(plugin);
  unsigned int slot_count = 1;

  if (tcore_server_load_modem_plugin(s,
          user_data->modem, MODEM_PLUGIN_NAME) == TCORE_RETURN_FAILURE)
  {
    err("Load Modem Plug-in - [FAIL]");
    return FALSE;
  }
  else
  {
    dbg("Load Modem Plug-in - [SUCCESS]");
  }

  tcore_server_send_notification(s,
      NULL, TNOTI_SERVER_ADDED_MODEM_PLUGIN_COMPLETED,
      sizeof(slot_count), &slot_count);

  return FALSE;
}

static gboolean on_load()
{
  dbg("LOAD!!!");

  return TRUE;
}

static gboolean on_init(TcorePlugin *plugin)
{
  PluginData *user_data;
  Server *s = tcore_plugin_ref_server(plugin);

  dbg("INIT!!!");

  /* User Data for Modem Interface Plug-in */
  user_data = g_try_new0(PluginData, 1);
  if (user_data == NULL)
  {
    err(" Failed to allocate memory for Plugin data");
    return FALSE;
  }

  /* Register to server */
  user_data->modem = tcore_server_register_modem(s, plugin);
  if (user_data->modem == NULL)
  {
    err(" Registration Failed");
    g_free(user_data);
    return FALSE;
  }

  user_data->hal = tcore_hal_new(plugin, "hal", &hal_ops, TCORE_HAL_MODE_CUSTOM);
  if (!user_data->hal)
  {
    tcore_server_unregister_modem(s, user_data->modem);
    g_free(user_data);
    return FALSE;
  }

  /* Link custom data to HAL user data */
  tcore_plugin_link_user_data(plugin, user_data);

  tcore_hal_set_power_state(user_data->hal, TRUE);

  g_timeout_add_full(G_PRIORITY_HIGH, SERVER_INIT_WAIT_TIMEOUT,
      load_modem_plugin, plugin, 0);

  dbg("Init - Successful");

  return TRUE;
}

static void on_unload(TcorePlugin *plugin)
{
  PluginData *user_data = tcore_plugin_ref_user_data(plugin);
  Server *s = tcore_plugin_ref_server(plugin);

  if (user_data == NULL)
  {
    return;
  }

  tcore_hal_free(user_data->hal);
  tcore_server_unregister_modem(s, user_data->modem);

  dbg(" Unregistered from Server");
  g_free(user_data);
}

/* QMI plug-in descriptor */
struct tcore_plugin_define_desc plugin_define_desc = {
  .name = "QMIHAL",
  .priority = TCORE_PLUGIN_PRIORITY_HIGH,
  .version = 1,
  .load = on_load,
  .init = on_init,
  .unload = on_unload
};
