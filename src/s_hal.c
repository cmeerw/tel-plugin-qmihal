/*
 * tel-plugin-qmi
 *
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Christof Meerwald <cmeerw@cmeerw.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <glib.h>

#include <tzplatform_config.h>
#include <tcore.h>
#include <user_request.h>
#include <core_object.h>
#include <plugin.h>
#include <server.h>
#include <hal.h>

#include "s_common.h"
#include "s_hal.h"


static TReturn hal_power(TcoreHal *hal, gboolean enable)
{
  struct PluginData *user_data;

  user_data = tcore_hal_ref_user_data(hal);
  if (user_data == NULL) {
    err(" User data is NULL");
    return TCORE_RETURN_FAILURE;
  }

  if (enable == TRUE)
  {
    /* Set Power State - ON */
    tcore_hal_set_power_state(hal, TRUE);
  }
  else
  {
    /* Set Power state - OFF */
    tcore_hal_set_power_state(hal, FALSE);
  }

  return TCORE_RETURN_SUCCESS;
}

/** HAL operations */
static struct tcore_hal_operations hal_ops = {
  .power = hal_power,
  .send = NULL,
  .setup_netif = NULL,
};

gboolean s_hal_init(TcorePlugin *p, PluginData *user_data)
{
  user_data->hal = tcore_hal_new(p, "hal", &hal_ops, TCORE_HAL_MODE_CUSTOM);
  if (!user_data->hal) {
    return FALSE;
  }

  tcore_hal_set_power_state(user_data->hal, TRUE);

  return TRUE;
}

void s_hal_exit(TcorePlugin *p)
{
  PluginData *user_data;

  if (!p) {
    err("Plugin is NULL");
    return;
  }

  user_data = tcore_plugin_ref_user_data(p);
  tcore_hal_free(user_data->hal);
}
