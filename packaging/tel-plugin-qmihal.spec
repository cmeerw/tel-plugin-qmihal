%define major 0
%define minor 1
%define patchlevel 81

Name:              tel-plugin-qmihal
Version:           %{major}.%{minor}.%{patchlevel}
Release:           1
License:           Apache-2.0
Summary:           Telephony QMI HAL library
Group:             System/Libraries
Source0:           tel-plugin-qmihal-%{version}.tar.gz

%if "%{profile}" == "tv"
ExcludeArch: %{arm} %ix86 x86_64
%endif

%if "%{_with_emulator}" != "1"
ExcludeArch: %{arm} aarch64
%endif

BuildRequires:     cmake
BuildRequires:     pkgconfig(glib-2.0)
BuildRequires:     pkgconfig(qmi-glib)
BuildRequires:     pkgconfig(dlog)
BuildRequires:     pkgconfig(tcore)
BuildRequires:     pkgconfig(libtzplatform-config)
Requires(post):    /sbin/ldconfig
Requires(postun):  /sbin/ldconfig

%description
Telephony QMI library

%prep
%setup -q

%build
cmake . -DCMAKE_INSTALL_PREFIX=%{_prefix} \
	-DLIB_INSTALL_DIR=%{_libdir}
make %{?_smp_mflags}

%post
/sbin/ldconfig

%postun -p /sbin/ldconfig

%install
%make_install

%files
%manifest tel-plugin-qmihal.manifest
%defattr(644,root,root,-)
#%doc COPYING
%{_libdir}/telephony/plugins/modems/qmihal-plugin*
%license LICENSE
